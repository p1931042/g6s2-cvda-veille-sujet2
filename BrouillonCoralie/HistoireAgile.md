## Histoire des méthodes agiles

Dans les années 90, Lauri Koskela, un étudiant finlandais, remarque que la majorité des projets de construction échouait. Il observe une différence entre les projets qui réussissait et ceux qui échouait. 

Il fera le constat que le projet ne satisfait pas le client, que les délais ne sont pas respectés et que le budget est dépassé (facteur 2 voire 3 et même 10). On pourrait penser que ce sont les aléas qui font échouer les projets, c’est totalement faux. La cause de ses dérives, est qu’il y a beaucoup d’incertitude qu’on ne veut pas voir. 

Le dernier constat fut que les projets qui réussissait avaient une gestion de projet atypique. Les personnes qui dirigeaient les projets fructueux, privilégiait le collectif et l’équipe, la proximité avec le client, et la flexibilité.

L’agilité va s’expandre au niveau informatique car elle souffrait des mêmes mots. L’agilité venait palier les lacunes de gestion de projet existante. 

Dans le domaine de l’informatique le temps entre la demande et la production de l’application était de d’au minimum 3 ans. Ce délai de 3 ans était le minimum, dans le domaine aérospatial et de la défense, ce délai pouvait s’étendre jusqu’à 20 ans. Le programme de la navette spatiale de 1982, a été commencé en 1960. 20 ans pour développer un logiciel complexe, à l’époque, était extrême mais pas inhabituel.

Jon Kern, ingénieur dans l’aérospatiale, chercha un moyen d’améliorer les process de développement, car il fut frustré de l’attente et par les décisions qui ne pouvait pas être modifier dès que le projet fut commencé. 

Il était un des 17 experts qui se rencontraient pour parler des façons de développer un projet, et des logiciels plus simplement. Un jour, ces experts se sont réunis en 2001 pour définir les principes fondamentaux des méthodes Agiles. Cette réunion se nomme la réunion Snowbird en Utah, et en découla le manifeste Agile. 
