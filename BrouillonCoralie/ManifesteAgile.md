## Manifeste Agile

Le manifeste Agile est la création de 17 experts du développement logiciel. Il fut créé en 2001, et a pour but de répertorier les points communs des méthodes Agiles. Il faut se rappeler que le manifeste Agile n'est absolument pas le père des méthodes Agiles.

Ce manifeste est composé de 4 valeurs et 12 principes. Elles peuvent être sujet à interprétation.  

Les 4 valeurs sont les suivantes :  

* ​<font size=2>Les individus et leurs interactions PLUS que les processus et les outils</font>
  
		La nouvelle différence entre les méthodes et les méthodes trad, c’est que les besoins du client sont au centre du projet. Aussi la communication au sein de l’équipe est primordiale.
		Les outils et les individus répondent aux besoins de l’entreprise et sont ce sont eux qui pilotent les process.

* <font size=2>​Des logiciels opérationnels PLUS qu’une documentation exhaustive</font>

	 	 Lorsqu’on pense à une documentation, on a une image d’un document compliqué rempli de détails technique, longue, exigent, détaillé, avec les essais, et beaucoup d’autres informations. Par le passé, elles étaient comme ça.
	 	Avec l’agilité, il y a une rationalisation de la documentation sous une forme où les développeurs trouve les informations dont ils ont besoin facilement. 

* <font size=2>​La collaboration avec les clients PLUS que la négociation contractuelle</font>
  
	   Le manifeste agile décrit un client comme une personne qui s’engage dans le processus de développement et de fabrication. Cela rend le développement plus facile car on répond aux besoins du client.
* <font size=2>​L’adaptation au changement PLUS que le suivi d’un plan</font>
  		
		Dans les méthodes traditionnellement, le changement est synonyme de dépenses. Avec agile c’est le contraire.
	 	Le changement permet d’enrichir et améliorer le projet, d’ajouter des fonctionnalité et reporté des priorités




Et les 12 principes sont les suivants :
1. ​<font size=2>Notre plus haute priorité est de satisfaire le client en livrant rapidement et régulièrement des fonctionnalités à grande valeur ajoutée.</font>


		Ce principe est le plus important des méthodes agiles. Pour savoir si le client est content, il faut l’impliquer dans le projet, dans le développement et dans toutes étapes du projet.

2. <font size=2>Accueillez positivement les changements de besoins, même tard dans le projet. Les processus Agiles exploitent le changement pour donner un avantage compétitif au client.</font> 


		Ici ce principe prône l’organisation, et le changement. S’allégez des phases amont de conception et de définition du projet car de toute façon ceux-ci changeront au cours du projet.

		Il faut être organisé pour bien accueillir le changement. Le client est important au milieu du projet car vaut mieux se tromper tôt que tard, il est préférable que le client indique lorsqu’on se trompe ou change des fonctionnalités au début du projet pour que tout le monde soit sur la même longueur d’onde.

		Chaque changement doit être traité comme n’importe quelle fonctionnalité, elle sera analysée, puis chiffrée pour ensuite être priorisée.  


3. ​<font size=2>Livrez fréquemment un logiciel fonctionnel, dans des cycles de quelques semaines à quelques mois, avec une préférence pour les plus courts.</font>
   

		Découper le cycle du projet en petit logiciel ne suffit pas. Ces découpages doivent réfléchit, il faut qu’ils aient une ou plusieurs livraisons de features fonctionnelles. Ces découpages s’appellent des sprints. Le principal est de faire des livraisons valide. 

		Si cette livraison a besoin de plusieurs sprints, alors allongez la durée de vos sprints. Au contraire si lors du sprint, vous avez fait 15 mises à jour et couvert 3 sujets, raccourcissez-les. Le client pourra être perdu ou pourra avoir peur, dans le premier cas il pensera que le projet a du retard et dans l’autre cas il sera débordé par la quantité de fonctionnalité à valider.


4. ​<font size=2>Les utilisateurs ou leurs représentants et les développeurs doivent travailler ensemble quotidiennement tout au long du projet.</font>
   
      
		La communication est importante au sein d’un projet, sans prendre en compte les mails froids sans âme. L’agencement de l’espace de travail peut être utile dans ce principe.

		Mieux regrouper les personnes d’un même projet et non par métier au sein de l’entreprise, avantage la communication au sein d’un même projet. Mais encore les parties prenantes d’un projet doivent s’entendre, échanger et travailler ensemble. 
		
		Des outils, rituels et coutumes peuvent faciliter cette collaboration.


5. ​<font size=2>Réalisez les projets avec des personnes motivées. Fournissez-leur l’environnement et le soutien dont elles ont besoin et faites-leur confiance pour atteindre les objectifs fixés.</font>  
   
		La confiance mutuelle est un point essentiel ici. Ce principe se base sur la transparence, le micro-management est à bannir.

		Les personnes ne sont pas des ressources interchangeables. Elles ont besoin de temps pour se connaitre et pour se faire confiance.
 
6. ​<font size=2>La méthode la plus simple et la plus efficace pour transmettre de l’information à l'équipe de développement et à l’intérieur de celle-ci est le dialogue en face à face. </font> 

		Parler est ici de rigueur. Privilégiez l’oral, et limitez l’écrit.

		C’est frustrant de voir deux personnes dans la même pièce, s’envoyer des mails ou de parler par un moyen de communication autre que le face à face, sauf si c’est pour demander la permission de venir se parler. Cela permet de savoir si la personne est occupée.

7. ​<font size=2>Un logiciel fonctionnel est la principale mesure de progression d'un projet.</font> 
   	
	    La meilleure méthode pour mesurer la progression d’un projet est de créer des points qui peuvent être mesuré. Il ne faut pas se concentrer que sur les bugs, ou les sprints, ces critères ne font pas tout le projet.
	
8. ​<font size=2>Les processus agiles encouragent un rythme de développement soutenable. Ensemble, les commanditaires, les développeurs et les utilisateurs devraient être capables de maintenir indéfiniment un rythme constant.</font> 
 
		Grace au méthodes agiles, on développe une plus grande adaptabilité. On ne doit pas tout interrompre, ou paniquer suite à une demande du client. Si ce cas arrive cela veut dire que vous n’êtes pas agile. 

		Le client est un membre de l’équipe : Il ne faut pas lui donner des quantités de fonctionnalité à valider. Ce rythme serait insoutenable pour lui.


9.  <font size=2>Une attention continue à l'excellence technique et à un bon design.</font> 

		 Essayer d’assembler des bouts de code écrit par différente personne sans problème, est presque impossible. Il faut demander aux équipes de s’occuper de la conception avant de coder.

		 Ecrire les spécificités détaillées, est une bonne solution. On n’écrit pas la documentation.


10.  ​<font size=2>La simplicité – c’est-à-dire l’art de minimiser la quantité de travail inutile – est essentielle.</font> 

		  Simplifier au maximum le projet, pour ne pas viser trop haut dès le début et pour que tout le monde (même le client) puisse comprendre le projet.

		  Ne pas viser la fin du projet directement, mais d’abord une première étape pour ne pas se décourager. Diviser le projet et divisez les étapes s’il le faut, cela sera en fonction des capacités des personnes dans le projet. Le but est de faire les étapes rapidement, plus les étapes seront simples plus elles seront rapides.

		  Cela permet de garder une documentation du projet simple, de ne pas s’embarrasser de réunions parasite et de veiller que personne ne se noient sous des taches complexe et/ou inutile.


11. ​<font size=2>Les meilleures architectures, spécifications et conceptions émergent d'équipes auto-organisées.</font>

		 La contrainte n’est pas très appréciée ici, car travailler sous la contrainte rend l’équipe moins performante. C’est pour cela qu’il faut les laisser s’autogérer.

		 Une équipe qui travail vers un objectif commun, de leur plein gré et par envie, sera plus efficace et plus fiable.

12. ​<font size=2>À intervalles réguliers, l'équipe réfléchit aux moyens possibles de devenir plus efficace. Puis elle s'adapte et modifie son fonctionnement en conséquence.</font>
    
		 La meilleure méthode, est d’avoir un groupe en constante évolution. Pour cela il faut être assidue, engagé et être à la recherche de moyen d’amélioration de la productivité. 

		 De temps en temps, même si c’est fastidieux, faire une réunion rétrospective. Elle passe facilement aux oubliettes car c’est à la fin du sprint, souvent le vendredi là ou tout le monde est fatigué. C’est une erreur car on ne peut pas s’améliorer en arrière.  

