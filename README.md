# g6s2-cvda-veille-sujet2

Méthodes agiles de gestion de projet : Scrum, Kanban, XP, BDD, TDD
par Gaëlle Bouchenot, Romain Dupré, Bastien Perrin, Coralie Bouhana, Océane Stchetinine et Loïc Perret 

Répartition :  
Définition des méthodes agiles => Bastien  
Histoire des méthodes agiles => Coralie  
Manifeste pour le développement des méthodes agiles => Coralie  
Kanban => Gaëlle  
Scrum => Romain  
XP => Loïc  
TDD => Océane  
BDD => Bastien / Océane
Conclusion => Loïc
