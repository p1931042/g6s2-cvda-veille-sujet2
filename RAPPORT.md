# Généralités sur les méthodes Agiles

## Définition

Une méthode agile est une approche collaborative, qui permet de prendre en compte les besoins d’un client : les besoins initiaux ainsi que les besoins liés à l'évolution du projet. Ces méthodes permettent de se répartir les tâches au sein d’un groupe sur le court terme, et permettent également une meilleure autonomie des équipes.

## Histoire des méthodes agiles

Dans les années 90, Lauri Koskela, un étudiant finlandais, remarque que la majorité des projets de construction échouent. Il observe une différence entre les projets qui réussissent et ceux qui échouent.

Il fait le constat que les projets ne satisfont pas les clients, que les délais ne sont pas respectés et que les budgets sont dépassés (facteur 2 voire 3 et même 10). On pourrait penser que ce sont les aléas qui font échouer les projets, mais c’est totalement faux. La raison de ces dérives est qu’il y a beaucoup d’incertitudes qu’on ne peut pas voir.

Le dernier constat fut que les projets qui réussissaient avaient une gestion de projet atypique. Les personnes qui dirigeaient les projets fructueux privilégiaient le collectif et l’équipe, la proximité avec le client, et la flexibilité.

L’agilité va s’étendre au niveau informatique car celle-ci souffrait alors des mêmes maux. L’agilité venait palier les lacunes de gestion de projet existantes.

Dans le domaine de l’informatique, le temps entre la demande et la production de l’application était d’au minimum 3 ans. Dans les domaines aérospatial et de la défense, ce délai pouvait s’étendre jusqu’à 20 ans. Le programme de la navette spatiale de 1982 a été commencé en... 1960. 20 ans pour développer un logiciel complexe, à l’époque, était extrême mais pas inhabituel.

Jon Kern, ingénieur dans l’aérospatiale, chercha un moyen d’améliorer les processus de développement, car il était frustré par l’attente et par les décisions qui ne pouvaient pas être modifiées dès lors que le projet était commencé.

Joe Kern est l'un des 17 experts qui se sont rencontrés pour parler des façons de gérer et développer un projet, et des logiciels plus simplement. En 2001, 17 experts se sont réunis pour définir les principes fondamentaux des méthodes Agiles. Cette réunion se nomme la réunion Snowbird, en Utah, et en découla le manifeste Agile.

## Manifeste Agile

Le manifeste Agile est la création de 17 experts du développement logiciel. Il fut créé en 2001, et a pour but de répertorier les points communs des méthodes Agiles. Il faut se rappeler que le manifeste Agile n'est absolument pas le père des méthodes Agiles.

Ce manifeste est composé de 4 valeurs et 12 principes. Ils peuvent être sujet à interprétation.  

Les 4 valeurs sont les suivantes :  

* ​<font size=2>Les individus et leurs interactions **PLUS** que les processus et les outils</font>
  
		La nouvelle différence entre les méthodes et les méthodes traditionnelles, c’est que les besoins du client sont au centre du projet. Aussi la communication au sein de l’équipe est primordiale.
		
		Les outils et les individus répondent aux besoins de l’entreprise et ce sont eux qui pilotent le processus.

* <font size=2>​Des logiciels opérationnels **PLUS** qu’une documentation exhaustive</font>

	 	Lorsqu’on pense à une documentation, on a une image d’un document long et compliqué rempli de détails techniques, avec les essais, et beaucoup d’autres informations. Par le passé, elles étaient comme ça.

	 	Avec l’agilité, il y a une rationalisation de la documentation sous une forme où les développeurs trouvent les informations dont ils ont besoin facilement. 

* <font size=2>​La collaboration avec les clients **PLUS** que la négociation contractuelle</font>
  
	   Le manifeste agile décrit un client comme une personne qui s’engage dans le processus de développement et de fabrication. 

	   Cela rend le développement plus facile car on répond aux besoins du client.
* <font size=2>​L’adaptation au changement **PLUS** que le suivi d’un plan</font>
  		
		Dans les méthodes traditionnelles, le changement est synonyme de dépenses. Avec les méthodes agiles, c’est le contraire.

	 	Le changement permet d’enrichir et améliorer le projet, d’ajouter des fonctionnalités et de reporter des tâches moins prioritaires.




Et les 12 principes sont les suivants :
1. ​<font size=2>Notre plus haute priorité est de satisfaire le client en livrant rapidement et régulièrement des fonctionnalités à grande valeur ajoutée.</font>


		Ce principe est le plus important des méthodes agiles. 

		Pour savoir si le client est content, il faut l’impliquer dans le projet, dans le développement et dans toutes les étapes du projet.

2. <font size=2>Accueillez positivement les changements de besoins, même tard dans le projet. Les processus Agiles exploitent le changement pour donner un avantage compétitif au client.</font> 


		Ce principe prône l’organisation, et le changement. S’alléger des phases amont de conception et de définition du projet, car de toute façon ceux-ci changeront au cours du projet.

		Il faut être organisé pour bien accueillir le changement. Le client est important au milieu du projet car il vaut mieux se tromper tôt que tard, il est préférable que le client indique lorsqu’on se trompe ou qu'on change des fonctionnalités au début du projet pour que tout le monde soit sur la même longueur d’onde.

		Chaque changement doit être traité comme n’importe quelle fonctionnalité : il sera analysé, puis chiffré, pour ensuite être priorisé.  


3. ​<font size=2>Livrez fréquemment un logiciel fonctionnel, dans des cycles de quelques semaines à quelques mois, avec une préférence pour les plus courts.</font>
   

		Découper le cycle du projet en petits logiciels ne suffit pas. Ces découpages doivent être réfléchis, il faut qu’ils aient une ou plusieurs livraisons de features fonctionnelles. Ces découpages s’appellent des sprints. Le principal est de faire des livraisons valides. 

		Si une livraison a besoin de plusieurs sprints, alors allongez la durée de vos sprints. Au contraire si lors du sprint, vous avez fait 15 mises à jour et couvert 3 sujets, raccourcissez-les. Le client pourra être perdu ou pourra avoir peur, dans le premier cas il pensera que le projet a du retard et dans l’autre cas il sera débordé par la quantité de fonctionnalités à valider.


4. ​<font size=2>Les utilisateurs ou leurs représentants et les développeurs doivent travailler ensemble quotidiennement tout au long du projet.</font>
   
      
		La communication est importante au sein d’un projet, sans prendre en compte les mails froids et sans âme. L’agencement de l’espace de travail peut être utile dans ce principe.

		Mieux regrouper les personnes d’un même projet et non par métier au sein de l’entreprise, avantager la communication au sein d’un même projet. Mais encore les parties prenantes d’un projet doivent s’entendre, échanger et travailler ensemble. 
		
		Des outils, rituels et coutumes peuvent faciliter cette collaboration.


5. ​<font size=2>Réalisez les projets avec des personnes motivées. Fournissez-leur l’environnement et le soutien dont elles ont besoin et faites-leur confiance pour atteindre les objectifs fixés.</font>  
   
		La confiance mutuelle est un point essentiel ici. Ce principe se base sur la transparence, le micro-management est à bannir.

		Les personnes ne sont pas des ressources interchangeables. Elles ont besoin de temps pour se connaitre et pour se faire confiance.
 
6. ​<font size=2>La méthode la plus simple et la plus efficace pour transmettre de l’information à l'équipe de développement et à l’intérieur de celle-ci est le dialogue en face à face. </font> 

		Parler est ici de rigueur. Privilégiez l’oral, et limitez l’écrit.

		Il est frustrant de voir deux personnes dans la même pièce s’envoyer des mails ou se parler autrement qu'en face à face, sauf si c’est pour demander la permission de venir se parler. Cela permet de savoir si la personne est occupée.

7. ​<font size=2>Un logiciel fonctionnel est la principale mesure de progression d'un projet.</font> 
   	
	    La meilleure méthode pour mesurer la progression d’un projet est de créer des points qui peuvent être mesurés. Il ne faut pas se concentrer que sur les bugs, ou les sprints, ces critères ne font pas tout le projet.
	
8. ​<font size=2>Les processus agiles encouragent un rythme de développement soutenable. Ensemble, les commanditaires, les développeurs et les utilisateurs devraient être capables de maintenir indéfiniment un rythme constant.</font> 
 
		Grace aux méthodes agiles, on développe une plus grande adaptabilité. On ne doit pas tout interrompre, ou paniquer suite à une demande du client. Si ce cas arrive cela veut dire que vous n’êtes pas agile. 

		Le client est un membre de l’équipe : il ne faut pas lui donner des quantités de fonctionnalités à valider. Ce rythme serait insoutenable pour lui.


9.  <font size=2>Une attention continue à l'excellence technique et à un bon design.</font> 

		 Essayer d’assembler des bouts de code écrits par différentes personnes sans problème relève de l'impossible. Il faut demander aux équipes de s’occuper de la conception avant de coder.

		 Ecrire les spécificités détaillées est une bonne solution. On n’écrit pas la documentation.


10.  ​<font size=2>La simplicité – c’est-à-dire l’art de minimiser la quantité de travail inutile – est essentielle.</font> 

		  Simplifier au maximum le projet, pour ne pas viser trop haut dès le début et pour que tout le monde (même le client) puisse comprendre le projet.

		  Ne pas viser la fin du projet directement, mais d’abord une première étape pour ne pas se décourager. Diviser le projet et diviser les étapes s’il le faut, cela  se fera en fonction des capacités des personnes impliquées dans le projet. Le but est de réaliser les étapes rapidement : plus les étapes seront simples, plus elles seront rapides.

		  Cela permet de garder une documentation du projet simple, de ne pas s’embarrasser de réunions parasites et de veiller que personne ne se noit sous des taches complexes et/ou inutiles.


11. ​<font size=2>Les meilleures architectures, spécifications et conceptions émergent d'équipes auto-organisées.</font>

		 La contrainte n’est pas très appréciée ici, car travailler sous la contrainte rend l’équipe moins performante. C’est pour cela qu’il faut les laisser s’autogérer.

		 Une équipe qui travaille vers un objectif commun, de son plein gré et par envie, sera plus efficace et plus fiable.

12. ​<font size=2>À intervalles réguliers, l'équipe réfléchit aux moyens possibles de devenir plus efficace. Puis elle s'adapte et modifie son fonctionnement en conséquence.</font>
    
		 La meilleure méthode, est d’avoir un groupe en constante évolution. Pour cela il faut être assidu, engagé et être à la recherche de moyens d’amélioration de la productivité. 

		 De temps en temps, même si c’est fastidieux, faire une réunion rétrospective. Elle passe facilement aux oubliettes car c’est à la fin du sprint, souvent le vendredi là ou tout le monde est fatigué. C’est une erreur car on ne peut pas s’améliorer en arrière.  



# Des méthodes qui se suivent et se complètent

## Kanban :

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Simple-kanban-board-.jpg/640px-Simple-kanban-board-.jpg)

[lien image](https://commons.wikimedia.org/wiki/File:Simple-kanban-board-.jpg)

Kanban, la première des méthodes agiles, a été créée dans les années 50 par Toyota, et a pour objectif initial d'améliorer la gestion des flux de matériaux et le contrôle des stocks. Cette méthode a par la suite été adaptée pour permettre à différents domaines, tel que celui du développement logiciel, d’améliorer leur gestion des tâches à réaliser.

Kanban, c'est avant tout un **système de planification**, qui repose sur la création d’un **tableau** (qu’il soit matériel ou modélisé à l’aide d’un logiciel tel que Trello). Celui-ci aura pour but de regrouper, par exemple toutes les missions, divisées sous forme de tâches, d’une équipe de développement. Chaque tâche est représentée par un carton ou un post-it, avec souvent un code couleur. Ces tâches seront ensuite généralement réparties dans 4 colonnes : 

- A Faire / To Do / Backlog  
- En cours / In progress  
- A tester / Testing  
- Fini / Done  


Pour autant, le nombre et le nom des colonnes sont souvent adaptés pour répondre au mieux aux besoins du projet. Mais l'adaptabilité du tableau ne s'arrête pas là, il est égalemet possible de rajouter des lignes (swimlanes), qui permettent de segmenter le projet, ou encore d'indiquer un ordre de priorité entre les tâches. Il est également possible de rajouter des règles, comme par exemple limiter le nombre de tâches dans une colonne, ou encore de définir quand une tâche est achevée ou quand elle passe à l'état suivant.

Les possibilités offertes par cette méthode sont quasiment infinies, d'autant plus que certains outils de gestion collaborative, tel que Framaboard, rajoutent encore plus de fonctionnalités, comme des sous-tâches ou encore un système de chronomètre. 

Cette méthodes encouragent également la mise en place de réunions régulières appelées “cadences”, pour par exemple faire évoluer les règles mises en place autour du tableau, mais aussi pour que la communication reste bien au coeur de cette méthode de gestion de projet.


**Avantages** de la méthode Kanban:
- Elle peut être mise en place dans une organisation déjà existante.  
- Elle permet de connaître facilement l’avancée des tâches à réaliser, et ainsi faciliter la **communication** au sein d'une équipe.
- Elle est extrêmement **modulable**, et peut donc être adaptée à n’importe quelle échelle.  
- Elle peut être complémentaire avec d'autres méthodes, tel que Scrum que nous allons vous présenter juste après.  
- Il est possible de rajouter de nouvelles tâches n'importe quand, ce qui la rend très **flexible**. 
- Limite le nombre de tâches en cours, et donc augmente la concentration des équipes sur celles-ci.  
- Ce système insite les équipes à se concentrer sur les tâches en cours, ce qui les rend normalement plus efficaces que si elles étaient dispersées sur plusieurs tâches.



**Inconvénients**
- Elle peut rapidement se complexifier lors de l’utilisation dans des grandes structures et ainsi devenir moins efficace pour faire passer l'information.  
- Il faut pouvoir diviser les différentes missions en tâches simples.  
- Elle ne matérialise pas les échéances, qu’il est donc facile de louper.  
- Elle ne montre pas les dépendances entre 2 tâches (si l’une doit être réalisée avant une autre).
- Comme il est toujours possible de rajouter de nouvelles tâches, il est possibles de se faire rapidement submerger. 
 


## Scrum :

Scrum est un terme qui signifie “mêlée” au rugby. Le saviez-vous ?  

Généralités :
SCRUM a été pensé par Jeff Sutherland dans les années 90 en se basant sur des entreprises japonaises. Cette méthode est utilisée dans beaucoup de domaines, pas seulement pour le développement informatique. Elle peut s'appliquer à tous les projets impliquant des produits finis.  

Un projet implémentant SCRUM commence avant tout avec **un cahier des charges** (backlog), géré par un « **Product Owner** » qui représente les intérêts du **client**, et définit les fonctionnalités du produit avec ce dernier. Le Product Owner est responsable du backlog (liste de tâches et spécificités du produit), il attribue aussi un degré de priorité à chaque tâche : une belle interface graphique peut faire partie du projet, mais elle ne sera pas traitée en priorité par rapport aux fonctionnalités du produit.  

Ensuite vient le **Sprint** pendant lequel les équipes vont développer un certain nombre de tâches durant un intervalle de temps prédéfini.
Chaque jour, les équipes réalisent une réunion appelée « **Daily Scrum** » pour garder un œil sur l’avancement global du projet en répondant aux questions : « Qu'est-ce que j'ai terminé depuis la dernière mêlée ? Qu'est-ce que j'aurai terminé d'ici la prochaine mêlée ? Quels obstacles me retardent ? ». Ces questions permettent aux développeurs de se situer dans l'avancement global du projet.  

Chaque Sprint se finit par une **rétrospective**, durant laquelle les équipes font leurs retours d’expériences et discutent des évolutions possibles pour le Sprint suivant.  

Pour résumer, un projet géré avec la methode Scrum se divise comme suit :  
  
Le **Product Owner** et le **client** définissent le **backlog**.  
L'**équipe** découpe le projet en plusieurs morceaux et attribue des tâches aux membres qui la composent.  
Un premier **Sprint** commence, les membres développent leur "morceau" de projet.  
Chaque jour, un **Daily Scrum** est réalisé afin de faire le point sur l'avancement du projet.  
A la fin du Sprint, l'équipe réalise un **refactor** et discute des améliorations possibles ainsi que des problèmes rencontrés.  
L'équipe propose enfin un **prototype** du produit au client qui leur fait un **retour**.  
Un nouveau **Sprint** commence avec les retours du client, en prenant en compte ses demandes, ainsi que ce qui a été dit lors du refactor.  
Les **Sprints** continuent jusqu'à ce que le produit soit terminé.  


**Management visuel** :  
Scrum repose aussi sur la transparence, dans le sens où certaines informations (comme l'organisation et l'avancement du projet) doivent être accessibles à tout le monde et en particulier aux équipes qui développent. C'est pourquoi Scrum peut avoir recours à un tableau qui permet l’organisation du projet ainsi que la consultation de son avancement.  
Un exemple de tableau :  

- **Ressources** : tâches récurrentes ou nécessaires (ex: définir des variables globales, constantes, etc...)
- **Backlog** : liste des tâches en cours (évolutive en fonction du client et des rétrospectives)   
- **To do** : planification des tâches à réaliser pendant le sprint
- **En cours** : tâches en cours
- **Contrôle de qualité** : tâches complétées et à vérifier
- **Fait** : tâches ayant passé le contrôle qualité
- **Bloqué** : finalisation dépendante d’une ou plusieurs autres tâches
  
Finalement on se rend compte que Kanban complète à merveille Scrum.
 
**Itérations** et **améliorations** sont les points forts de Scrum :
Présenter un prototype utilisable au client à la fin de chaque Sprint est essentiel, cela permet de recueillir des retours d’expériences utilisateurs tôt pendant le projet. De plus, le client reste impliqué dans le projet. Cela lui donne aussi la possibilité de demander des améliorations, préciser ses idées afin que le produit final lui corresponde réellement, plutôt que de recevoir directement le produit fini et de réaliser qu'il ne correspond pas ce que le client voulait. De plus, les rétrospectives régulières permettent d’améliorer l’efficacité de l’équipe durant le déroulement du projet.  
  
**L'inconvénient**, comme le reste de méthodes agiles, est que la communication peut devenir complexe si l'entreprise est trop grande, retardant ainsi les feedbacks des différentes équipes.  
Un autre problème de cette méthode est que des tests ne sont pas réalisés **systématiquement**, cela peut conduire à un dysfonctionnement du produit ou du prototype dans certains cas d'utilisation qui peuvent ne pas avoir été pensés par le développeur, ou pas vus lors du test du client. Le code peut donc être incomplet.  



## XP :

L’extreme programming est une méthode agile parmi les plus populaires, élaborée entre autres par Kent Beck, un informaticien américain, entre 1996 et 1999. Comme son nom l’indique, elle consiste en l’application la plus extrême de principes déjà existants à l’époque : prêter attention aux besoins du client, mettre en place un développement itératif et intégrer de manière continue les fonctionnalités.

XP s’appuie sur 5 valeurs :

	1. La simplicité :  
		Tout ce qui est demandé sera réalisé, mais pas plus.  
		Le développement sera morcelé en petites étapes afin de minimiser les erreurs.  
		Seules les solutions les plus simples seront retenues.
	2. La communication :  
		Tout le monde doit s’impliquer dans le projet, le travail d’équipe est essentiel.  
		Des réunions en face à face sont organisées chaque jour pour échanger sur l’avancée et trouver les meilleures solutions.
	3. Le feedback :  
		Toute proposition se doit d’être écoutée et prise en compte, pour que les modifications nécessaires soient apportées et ce, à chaque itération du développement.  
		L’échange avec le client doit être aussi fréquent que possible.
	4. Le respect :  
		Tout membre de l’équipe doit être respectueux des autres et être respecté pour sa contribution.  
		Il faut savoir accepter les critiques constructives sur son travail.  
		Les développeurs et le client doivent s’écouter mutuellement.
	5. Le courage :  
		Tout problème doit être communiqué, la progression est explicitement communiquée, et il faut être capable de s’adapter aux changements.

### Pratiques extrêmes

Comme dit précédemment, l’intérêt de XP réside dans l’application extrême de principes propres aux méthodes agiles. Parmi les pratiques poussées à l’extrême, découlant des 5 valeurs sur lesquelles s’appuie XP, on retrouve notamment :

- La **présence d’un représentant du client** sur le site, du début à la fin du projet. Elle est essentielle, car grâce à sa vision globale du résultat, il peut répondre rapidement aux questions de l’équipe en charge du projet.
- La **mise en place de tests fonctionnels**, parfois automatisés, au commencement de chaque itération. Ainsi, tout morceau de code ne peut être intégré au produit que s’il passe tous les tests.
- Le **refactoring** : le code est régulièrement retravaillé pour en améliorer la qualité, sans en modifier le comportement. Cela n’apporte rien de significant au client mais est d’une grande aide au développeur (meilleure lisibilité par exemple).
- Les **conventions de langage** permettent de faciliter le travail de chacun, en utilisant les mêmes termes, styles, règles de nommage pour les variables, les classes, les fichiers, etc. puisque chacun est libre de modifier toute portion du projet, même celles dont il ne s’est pas occupé.
- La **programmation en binôme**, avec un pilote qui écrit le code et un copilote qui peut relever des erreurs et suggérer des modifications. Les rôles et binômes sont souvent changés pour améliorer la communication et les connaissances de chacun.

Tout cela doit permettre à l’équipe de pouvoir travailler dans les meilleures conditions, car XP possède aussi un aspect humain. Le rythme doit donc être soutenable, les équipes n’ont pas à faire d’heures supplémentaires. Si cela est nécessaire, alors c’est le planning qui est revu.

### Avantages, inconvénients, limites… :

Les pratiques assez extrêmes d’XP lui confèrent de nombreux avantages, mais aussi quelques inconvénients à ne pas négliger.

**[+]** Les tests continus permettent d’éviter énormément d’erreurs, en les corrigeant très tôt dans le développement, cela dans le but d’avoir un produit stable et d’éviter de programmer inutilement.

**[+]** Le développement par itérations permet à un client n’ayant pas d’idée précise de son produit d’obtenir ce qu’il souhaite, grâce à son implication directe et constante dans la réalisation dudit produit...

**[+]** La programmation en binôme sert aussi à éviter des erreurs dans le code...

**[-]** ...mais deux personnes qui travaillent sur un même code simultanément, c’est aussi des coûts de production qui deviennent plus importants.

**[-]** De plus, si le client sait déjà exactement ce qu’il veut, alors XP ne sera pas la méthode la plus adaptée au développement du produit.

**[-]** Le bien-être des programmeurs se veut être préservé grâce à XP, en n’imposant aucune heure supplémentaire, or les deadlines très fréquentes peuvent tout de même créer un stress récurrent chez le développeur, ce qui est un tout autre problème…

**[-]** Plus généralement, XP atteint ses limites quand les équipes sont “trop grandes”, quand “le travail en binôme est impossible” ou quand “les retours sont longs et compliqués à obtenir”.

Ainsi, XP est une véritable opportunité pour les petits et moyens projets, mais nécessite de la rigueur, de l’autodiscipline et de la régularité. Si ces conditions sont remplies et que les principes de cette méthode sont correctement appliqués, alors il est possible d’avoir un produit d’une qualité exceptionnelle.

## Méthode TDD & BDD : 

![](https://blog.testlodge.com/wp-content/uploads/2018/04/tdd_v_bdd_cycle-1024x538.png)

[lien image](https://blog.testlodge.com/wp-content/uploads/2018/04/tdd_v_bdd_cycle-1024x538.png)

## TDD

La méthode TDD ou Test Driven Development est une méthode agile inspirée de la méthode XP. En effet, elle repose sur un des principes fondamentaux de cette dernière : **Le Test First** .
Au sein de cette méthode, le code source est dirigé par **les tests automatisés**. C'est-à-dire qu'avant même l'écriture de la première ligne de code, on écrit les tests. Ce fonctionnement est ce qui lui permet d’ailleurs de livrer des logiciels efficaces tout en ayant les vérifications au fur et à mesure.
Malgré les différents guides ou documentations qu’on peut trouver sur la méthode TDD, elle reste néanmoins aujourd’hui très peu utilisée car assez coûteuse. 
En effet, afin de mettre en place cette méthode, il faudrait au minimum deux personnes, pendant qu’une personne écrit les tests, l’autre écrit le code testé.

Pour résumer, les 3 grands principes sont : 
-    **Loi  1** : Vous devez écrire un test qui échoue avant de pouvoir écrire le code de production correspondant.
-    **Loi  2** : Vous devez écrire une seule assertion à la fois, qui fait échouer le test ou qui échoue à la compilation.
- 	 **Loi  3** : Vous devez écrire le minimum de code de production pour que l'assertion du test actuellement en échec soit satisfaite.

Détailler, ces différents principes nous donnent 5 étapes distinctes:

	1. Écrire un premier test
	2. Vérifier que ce dernier échoue.
	3. Écrire le code suffisant pour que le test passe.
	4. Vérifier que le test est correct.
	5. Optimiser le code et continuer de vérifier.

Et cela en boucle jusqu'à la **fin du développement**.

### Les Avantages

Cette méthode comme toute autre méthode a ses avantages et ses inconvénients. 
Du côté des avantages principaux qu'elle apporte, on peut en noter certains tels que :

- 	**L'aboutissement certain** : Comme on se focalise au fur et à mesure sur les besoins du projet, le TDD évite des modifications de code sans lien avec le but visé. 
- 	**Identification facile des erreurs** : Le TDD permet d’éviter les tests qui échouent sans qu’on puisse identifier la modification à l'origine vu qu’ils sont écrits en même temps.
- 	**La lisibilité** : Permet de s’approprier plus facilement n’importe quelle partie du code en vue de le faire évoluer, car chaque test ajouté dans la construction du logiciel 
- 	**Une livraison plus qu'efficace** : Elle permet de livrer une nouvelle version d’un logiciel avec un haut niveau de qualité dans les livrables, justifiée par la couverture et la pertinence des tests à sa construction
- 	**Un Gain de temps** : En effet,le TDD permet de gagner du temps lors de la maintenance, car les défauts doivent être faciles à trouver et sont détectés tôt dans le processus de développement (une méthode de production sans échec).

#### Les Inconvénients

Les Inconvénients restent certes nettement moins nombreux mais non négligeable :
- 	Ecrire des tests avant d’écrire le code implique de savoir et d’avoir définie en amont les exigences du projet et la **spécification du code**.
- 	Le TDD prendra probablement **plus de temp**s à code , même avec un gain de temps lors de la maintenance, certains bien fait de cette méthode ne sont visibles qu’a la maintenant du logiciel, ce qui peut déranger certaines constructions de projets.


## BDD
La méthode agile **BDD**, pour Behavior Driven Development a été formalisée en 2003 par Dan North. Cette méthode à été développer par extension à la méthode TDD.

La différence avec la méthode TDD est que le BDD consiste à **écrire les tests en langage littéral** et non plus en code. 
Cette différence permet d’être compris par tous et pas que au développeurs. Cette réécriture permet une meilleure compréhension entre product owner, les développeurs et les intervenants non technique.

Au niveau de la syntaxe on peut l’écrire en Anglais ou en Français avec les bases suivantes :
- **Etant donné** : Given
- **Quand** : When
- **Alors** : Then
- **Et** : And


### Les Avantages

Les avantages de cette méthodes sont multiples. En effet elle permet l’écriture des scénarios de manière **collective**, entre développeurs et client par exemple. Cette méthode permet également une **meilleure clarté**, en effet avec des exemples plus concrets, les scénarios sont mieux compris.
Le besoin réalisé est également plus proche de la demande car il y a une **meilleure compréhension** entre le client et l’équipe de développement.

## En conclusion
La méthode TDD est une méthode très avantageuse en **qualité** et en **lisibilité**, dans des cas où l'on peut se permettre de l'intégrer au projet. Elle reste néanmoins encore aujourd'hui difficile à mettre en place, que ce soit par le coût humain ou l'organisation presque infaillible qu'elle demande.
Du côté de la méthode BDD, elle se positionne simplement comment étant un **complément** de la méthode TDD qui apporte une meilleure écriture du code par la suite, ce qui peut apporter un point assez intéréssant sur certains projets.

# Conclusion

Pour résumer...  
* Kanban met surtout l’accent sur la communication (rien n’est mentionné au niveau des deadlines).  
* Scrum repose beaucoup sur les itérations et l'ajout successif des fonctionnalités terminées au projet, (principes des méthodes agiles : investissement du client de manière régulière, Sprint, réunion régulière après les sprints etc…)  
* XP est lui centralisé principalement autour de méthodes extrêmes de développement en gardant l'aspect communicatif essentiel.

Nous vous avons donc présenté 5 méthodes de gestion de projet agiles, qui s'opposent donc aux méthodes de gestion de projet dites traditionnelles par leur flexibilité, mais il en existe de nombreuses autres : RAD, RUP, FDD...